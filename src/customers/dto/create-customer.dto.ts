import { IsNotEmpty, IsPositive, Length, Matches } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
